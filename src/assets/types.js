/**
 * @typedef {Object} PokemonMin
 * @property {string} name
 * @property {string} url
 */

/**
 * @typedef {Object} PokeType
 * @property {number} slot
 * @property {Object} type
 * @property {string} type.name
 */

/**
 * @typedef {Object} PokemonChild
 * @property {Object} sprites
 * @property {string} sprites.front_default
 * @property {PokeType[]} types
 *
 * @typedef {PokemonMin & PokemonChild} Pokemon
 */

export {};
