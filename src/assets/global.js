const API_URL = "https://pokeapi.co/api/v2";
const FAV_KEY = "favorite-pokemons";

import * as Types from "/assets/types.js"; // eslint-disable-line

/**
 * Returns list of pokemon
 * @param {number} offset Where to start the query index
 * @param {number} limit Number of pokemons to bring
 * @returns {Promise<Types.PokemonMin[]>}
 */
async function fetchPokemonList(offset = 0, limit = 20) {
  try {
    const res = await fetch(`${API_URL}/pokemon?offset=${offset}&limit=${limit}`);
    const data = await res?.json?.();
    return data?.results || [];
  } catch (error) {
    console.log(error);
    return [];
  }
}

/**
 * Returns data for a given pokemon slug
 * @param {string} slug Where to start the query index
 * @returns {Promise<Types.Pokemon>}
 */
async function fetchPokemonItem(slug) {
  try {
    const res = await fetch(`${API_URL}/pokemon/${slug}`);
    const data = await res?.json?.();
    return data;
  } catch (error) {
    console.log(error);
  }
}

/**
 * Returns favorite pokemons from local storage
 * @returns {Types.Pokemon[]}
 */
function getFavoritePokemons() {
  try {
    const favoritesRaw = localStorage.getItem(FAV_KEY) || "[]";
    const favorites = JSON.parse(favoritesRaw);
    return favorites;
  } catch (error) {
    console.log(error);
    return [];
  }
}

/**
 * Returns the current status of a pokemon, if it's a favorite or not
 * @param {Types.Pokemon} pokemon
 * @returns {boolean}
 */
function isPokemonFavorite(pokemon) {
  try {
    const favorites = getFavoritePokemons();
    const isIn = favorites?.find((favPok) => favPok.name === pokemon.name);
    return !!isIn;
  } catch (error) {
    console.log(error);
    return false;
  }
}

/**
 * Toggles the status of a pokemon as favorite, adding or removing it from the favorites array
 * @param {Types.Pokemon} pokemon
 * @returns {boolean} Status of the operation, if it was successful or not
 */
function toggleFavoritePokemon(pokemon) {
  try {
    const favorites = getFavoritePokemons();
    const isIn = favorites?.find((favPok) => favPok.name === pokemon.name);
    let newFavorites = [];
    if (isIn) {
      newFavorites = favorites.filter((favPok) => favPok.name !== pokemon.name);
    } else {
      newFavorites = [...favorites, pokemon];
    }
    localStorage.setItem(FAV_KEY, JSON.stringify(newFavorites));
    window.dispatchEvent(new Event("storage"));
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
}

export {
  fetchPokemonList,
  fetchPokemonItem,
  getFavoritePokemons,
  isPokemonFavorite,
  toggleFavoritePokemon,
};
