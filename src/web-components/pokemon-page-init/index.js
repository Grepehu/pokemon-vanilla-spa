import { getParam } from "/assets/router.js";

class PokemonPageInit extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    const slug = getParam("slug");
    this.innerHTML = `
            <pokemon-item slug="${slug}"><pokemon-item>
        `;
  }
}

window.customElements.define("pokemon-page-init", PokemonPageInit);
