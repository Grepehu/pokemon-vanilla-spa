import { fetchPokemonItem, toggleFavoritePokemon, isPokemonFavorite } from "/assets/global.js";
import * as Types from "/assets/types.js"; // eslint-disable-line

class PokemonItem extends HTMLElement {
  /** @type {string} */
  slug;
  /** @type {Types.Pokemon} */
  pokemon;
  /** @type {HTMLButtonElement} */
  btn;

  constructor() {
    super();
    this.slug = this.getAttribute("slug");
  }

  connectedCallback() {
    this.initialRender();
    this.fetchPokemon();
  }

  async fetchPokemon() {
    const data = await fetchPokemonItem(this.slug);
    this.pokemon = data;
    this.render();
  }

  initialRender() {
    this.innerHTML = `
            <progress>Loading ...<progress>
        `;
  }

  render() {
    const template = document.querySelector("#pokemon-item");
    this.innerHTML = template.innerHTML
      .replace(/\[\[name\]\]/g, this.pokemon.name)
      .replace(/\[\[src\]\]/g, this.pokemon.sprites.front_default)
      .replace(/\[\[types\]\]/g, this.pokemon.types.map((type) => type.type.name).join(", "));

    this.btn = this.querySelector("button");
    this.addEvents();
    this.isFavorite();
  }

  addEvents() {
    this.toggleFavoriteEvent();
  }

  toggleFavoriteEvent() {
    this.btn?.addEventListener("click", () => {
      this.toggleFavorite();
    });
  }

  toggleFavorite() {
    toggleFavoritePokemon(this.pokemon);
    this.isFavorite();
  }

  isFavorite() {
    const isFav = isPokemonFavorite(this.pokemon);
    if (isFav) {
      this.btn.style.color = "#ff0000";
    } else {
      this.btn.style.color = "#000";
    }
  }
}

window.customElements.define("pokemon-item", PokemonItem);
