import { fetchPokemonList } from "/assets/global.js";

class PokemonList extends HTMLElement {
  /** @type {string[]} */
  pokemonList = [];
  /** @type {HTMLButtonElement} */
  btn;
  /** @type {HTMLUListElement} */
  ul;

  constructor() {
    super();
  }

  connectedCallback() {
    this.initialRender();
    this.fetchList();
  }

  async fetchList() {
    this.btn.innerHTML = "Loading ...";
    const data = await fetchPokemonList(this.pokemonList.length);
    this.pokemonList = this.pokemonList.concat(data.map((pokemon) => pokemon.name));
    this.renderPokemons();
  }

  initialRender() {
    const template = document.querySelector("#pokemon-list");
    this.innerHTML = template.innerHTML;
    this.btn = this.querySelector("button");
    this.ul = this.querySelector("ul");
    this.btn?.addEventListener?.("click", () => {
      this.fetchList();
    });
  }

  renderPokemons() {
    this.pokemonList.forEach((pokemon) => {
      const alreadyIn = this.ul.querySelector(`[slug="${pokemon}"]`);
      if (alreadyIn) return;
      const newPokemon = document.createElement("pokemon-item");
      newPokemon.setAttribute("slug", pokemon);
      this.ul.append(newPokemon.cloneNode());
      newPokemon.remove();
    });
    this.btn.innerHTML = "Load More";
  }
}

window.customElements.define("pokemon-list", PokemonList);
