import { getFavoritePokemons } from "/assets/global.js";
import * as Types from "/assets/types.js"; // eslint-disable-line

class PokemonFavList extends HTMLElement {
  /** @type {Types.Pokemon[]} */
  pokemonList = [];
  /** @type {HTMLUListElement} */
  ul;

  constructor() {
    super();
  }

  connectedCallback() {
    this.initialRender();
    this.fetchList();
    this.listenFavoritesChanged();
  }

  fetchList() {
    this.pokemonList = getFavoritePokemons();
    this.renderPokemons();
  }

  initialRender() {
    const template = document.querySelector("#pokemon-fav-list");
    this.innerHTML = template.innerHTML;
    this.ul = this.querySelector("ul");
  }

  renderPokemons() {
    this.ul.innerHTML = "";
    this.ul.querySelectorAll("pokemon-item").forEach((alreadyInPoke) => {
      const slug = alreadyInPoke.getAttribute("slug");
      if (this.pokemonList.some((pok) => pok.name === slug)) return;
      alreadyInPoke.remove();
    });
    this.pokemonList.forEach((pokemon) => {
      const alreadyIn = this.ul.querySelector(`[slug="${pokemon.name}"]`);
      if (alreadyIn) return;
      const newPokemon = document.createElement("pokemon-item");
      newPokemon.setAttribute("slug", pokemon.name);
      this.ul.append(newPokemon.cloneNode());
      newPokemon.remove();
    });
  }

  listenFavoritesChanged() {
    window.addEventListener("storage", () => {
      this.fetchList();
    });
  }
}

window.customElements.define("pokemon-fav-list", PokemonFavList);
