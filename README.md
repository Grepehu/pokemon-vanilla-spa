# Pokemon Favorite Vanilla SPA

This is a test repository with a goal of creating an SPA where you fetch a list of pokemons, save them to your favorites and you can navigate between pages all using the client-side router [Web Router](https://gitlab.com/Grepehu/client-side-web-component-router).

Everything in this repository uses only native JS technologies such as Web Components, including the client-side navigation.

## Testing

This repository uses Elevent to facilitate the process of importing HTML chunks into the main one, to test you only need to have NodeJS 18+ installed, run `yarn install` in the root folder, then `yarn serve` to run a testing server, where you can check the router being used in real time at `http://localhost:8080`.
